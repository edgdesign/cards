require "cards/version"
require 'cards/deck'
require 'cards/ride_the_bus'
module Cards
	class Error < StandardError; end
	# Your code goes here...

	def self.print_ascii cards
		cards = cards.map { |c| c.to_ascii.lines }
		cards = cards[0].zip *cards[1..-1]
		cards = cards.flat_map do |line|
			line.map(&:chomp).join << "\n"
		end.join
		cards
	end
end
