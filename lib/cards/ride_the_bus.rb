module Cards
	class RideTheBus
		PHASES=[:black_or_red, :above_or_below, :inside_or_outside, :game_over]
 
		class Turn
			def initialize phase, cards, guess
				@phase, @cards, @guess = phase, cards, guess
			end

			def won?
				send @phase, @guess
			end

			private

			def inside_or_outside guess

				hand = [@cards[0].rank, @cards[1].rank].sort

				if guess == 'inside' && @cards[2].rank.between?(*hand)
					true
				elsif guess == 'outside' && !@cards[2].rank.between?(*hand)
					true
				else
					false
				end
			end

			def black_or_red guess
				if guess == 'black' && @cards[0].black?
					true
				elsif guess == 'red' && @cards[0].red?
					true
				else
					false
				end
			end

			def above_or_below guess
				if guess == 'above' && @cards[1].rank >= @cards[0].rank
					true
				elsif guess == 'below' && @cards[1].rank < @cards[0].rank
					true
				else
					false
				end
			end
		end

		attr_reader :cards, :turns, :phase
		def initialize
			@deck = Deck.new
			@deck.shuffle!
			@cards = []
			@turns = []
			@phases = PHASES.each
			@phase = @phases.next
		end

		def play guess
			return if complete?

			cards << @deck.draw
			@turns << Turn.new(phase, cards.clone, guess)

			@phase = @phases.next if won?
			@phase = :game_over unless won?

			won?
		end

		def complete?
			phase == :game_over
		end

		def won?
			@turns.all? &:won?
		end

		def to_s
			<<-TEXT
#{cards.map(&:to_s).join(' ')}
#{phase}
TEXT
		end
	end
end