module Cards
	class Deck
		RANKS=[:ace, :king, :queen, :jack, :ten, :nine, :eight, :seven, :six, :five, :four, :three, :two] 
		SUITS=[:spade, :heart, :diamond, :club]

		class Suit
			include Comparable

			attr_reader :suit
			def initialize(suit)
				@suit = suit
			end

			def to_s
				{
					spade: "\u2660",
					heart: "\u2665",
					diamond: "\u2666", 
					club: "\u2663"
				}[suit]
			end

			def red?
				[:heart, :diamond].include? @suit
			end

			def black?
				[:spade, :club].include? @suit
			end

			def <=> other
				return 0 if suit == other.suit

				SUITS.index(suit) < SUITS.index(other.suit) ? 1 : -1
			end
		end

		class Rank
			include Comparable

			attr_reader :rank
			def initialize(rank)
				@rank = rank
			end

			def to_s
				{
					ace: 'A',
					king: 'K',
					queen: 'Q',
					jack: 'J',
					ten: '10',
					nine: '9',
					eight: '8',
					seven: '7',
					six: '6',
					five: '5',
					four: '4',
					three: '3',
					two: '2'
				}[rank]
			end

			def face?
				[:ace, :king, :queen, :jack].include? @rank
			end

			def <=> other
				return 0 if rank == other.rank

				RANKS.index(rank) < RANKS.index(other.rank) ? 1 : -1
			end
		end

		class Card
			RANK_UNICODE = {
				ace: '1',
				king: 'E',
				queen: 'D',
				jack: 'B',
				ten: 'A',
				nine: '9',
				eight: '8',
				seven: '7',
				six: '6',
				five: '5',
				four: '4',
				three: '3',
				two: '2'
			}

			SUIT_UNICODE = {
				spade: 'A',
				heart: 'B',
				diamond: 'C',
				club: 'D'
			}

			include Comparable
			attr_accessor :rank, :suit
			def initialize rank, suit
				@rank, @suit = Rank.new(rank), Suit.new(suit)
			end

			def black?
				@suit.black?
			end

			def red?
				@suit.red?
			end

			def to_s
				["1f0#{SUIT_UNICODE[suit.suit]}#{RANK_UNICODE[rank.rank]}".hex].pack('U*')
			end

			def to_ascii
				card_template = <<-TEXT
 _________ 
|%-2s       |
|         |
|         |
|    %s    |
|         |
|         |
|_______%2s|
TEXT

				card_template % [rank.to_s, suit.to_s, rank.to_s ]
			end

			def <=> other
				return 0 if rank == other.rank && suit == other.suit
				return 1 if rank >= other.rank && suit >= other.suit
				return -1
			end
		end


		attr_accessor :cards
		def initialize
			@cards = RANKS.flat_map do |rank|
				SUITS.map do |suit|
					Card.new(rank, suit)
				end
			end
		end

		def draw
			@cards.shift
		end

		def sort!
			@cards.sort!
		end

		def shuffle!
			@cards.shuffle!
		end
	end
end